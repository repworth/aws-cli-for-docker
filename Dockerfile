FROM alpine:3.3

RUN \
  apk -U add groff less python py-pip && \
  pip install awscli==1.10.1 && \
  apk --purge -v del py-pip && \
  rm /var/cache/apk/*
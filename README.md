Docker container with: 

- AWS CLI Tools

## Scripts
Open shell with provided environment variables:
```
docker \
  run \
  -it \
  -e AWS_ACCESS_KEY_ID \
  -e AWS_SECRET_ACCESS_KEY \ 
  -e AWS_DEFAULT_REGION \
  repworth/aws-cli
```